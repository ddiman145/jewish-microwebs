<?php

namespace AkipShoptetImport\Model\CategoryRepository;
class Category
{
    private $id;

    private $name;

    private $imgUrl;

    private $heurekaCategory;

    private $shotetDataUrl;

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getImgUrl()
    {
        return $this->imgUrl;
    }

    public function getHeurekaCategory()
    {
        return $this->heurekaCategory;
    }

    public function getShotetDataUrl()
    {
        $this->shotetDataUrl;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function setImgUrl($imgUrl)
    {
        $this->imgUrl = $imgUrl;
        return $this;
    }

    public function setHeurekaCategory($heurekaCategory)
    {
        $this->heurekaCategory = $heurekaCategory;
        return $this;
    }

    public function setShoptetDataUrl($shoptetDataUrl)
    {
        $this->shotetDataUrl = $shoptetDataUrl;
        return $this;
    }

    public function load($data)
    {
        if (isset($data['id'])) {
            $this->id = $data['id'];
        }
        $this->setName($data['name']);
        $this->setImgUrl($data['img_url']);
        $this->setShoptetDataUrl($data['img_url']);
        $this->setHeurekaCategory($data['heureka_category']);
        return $this;
    }
}
