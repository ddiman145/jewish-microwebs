<?php
//require_once(AkipShoptetImport::getPluginDir() . '/Model/Category.php');
//namespace AkipShoptetImport\Repository\CategoryRepository;
//use AkipShoptetImport\Model\CategoryRepository\Category;

class CategoryRepository
{
    const CATEGORY_TABLE = 'akipshotetimport_category';

    public function __construct()
    {
    }

    public static function getCategoryTableName()
    {
        global $wpdb;
        return $wpdb->prefix . self::CATEGORY_TABLE;
    }

    public function getAll()
    {
        global $wpdb;
//        $data = $wpdb->query("SELECT * FROM " . self::getCategoryTableName());
        $data = $wpdb->get_results("SELECT * FROM " . self::getCategoryTableName());
        return $data;
    }

}
