<?php
/**
 * Plugin name:     AkipShoptetImport
 * Description:     Import zboži ze Shoptetu
 * Version:         1.0
 * Author:          AKIP Solutions s.r.o.
 * Author URI:      https://akip.cz/
 * Test Domain:     akipshoptetimport
 * Domain Path:     /lang
 */
//namespace AkipShoptetImport\App;

// SECURITY REASON - check if wp is initialized
if (!function_exists('add_action')) {
    die();
}
require_once(dirname(__FILE__) . "/Model/Product.php");
$autoloader = dirname( __FILE__ ) . '/vendor/autoload.php';
if ( is_readable( $autoloader ) ) {
    require_once $autoloader;
}
use Automattic\WooCommerce\Client;

class AkipShoptetImport
{
    const PRODUCT_TALBE = 'akipshotetimport_product';

    const _URL = 'http://localhost';
    const CONSUMER_KEY = 'ck_67b3d6465c6266674d2720f129f18da3268d775b';
    const CONSUMER_SECRET = 'cs_8ce32276288c984f5ce0ae9d7b9617ef7d4c5240';

    public static function getProductTable()
    {
        global $wpdb;
        return $wpdb->prefix . self::PRODUCT_TALBE;
    }

    public static function getPluginDir()
    {
        return (plugins_url('', __FILE__));
    }

    public function register()
    {
        add_action('init', [$this, 'custom_post_type']);
        add_action('admin_menu', [$this, 'akipshoptetimport_show_nav_item']);
        add_action('admin_enqueue_scripts', [$this, 'akipshoptetimport_load_assets_admin']);
        add_action('wp_enqueue_scripts', [$this, 'akipshoptetimport_load_assets_front']);
        add_action('save_post', [$this, 'akipshoptetimport_save']);
    }

    static function activation()
    {
        flush_rewrite_rules();
    }

    static function deactivation()
    {
        flush_rewrite_rules();
    }

    public function akipshoptetimport_create_plugin_database_table()
    {
        global $wpdb;
        $wp_track_table = self::getProductTable();
        if ($wpdb->get_var("show tables like '$wp_track_table'") != $wp_track_table) {
            $sql = "CREATE TABLE `{$wp_track_table}` 
                    ( 
                        `id` INT NOT NULL AUTO_INCREMENT 
                        , `category_id` BIGINT UNSIGNED NOT NULL
                        , `name` VARCHAR(255) NOT NULL 
                        , `price` FLOAT NOT NULL 
                        , `price_from` FLOAT NULL 
                        , `price_to` FLOAT NULL 
                        , `origin_url` VARCHAR(500) NOT NULL 
                        , `description` LONGTEXT NULL 
                        , `short_description` LONGTEXT NULL 
                        , `images` LONGTEXT NULL 
                        , `variants` LONGTEXT NULL 
                        , PRIMARY KEY (`id`)
                    ) ENGINE = InnoDB";
            require_once(ABSPATH . '/wp-admin/includes/upgrade.php');
            dbDelta($sql);
        }
    }

    public function akipshoptetimport_show_nav_item()
    {
        add_menu_page(
            esc_html__('Produkty', 'akipshoptetimport'),
            esc_html__('Produkty', 'akipshoptetimport'),
            'manage_options',
            'shotetimport-options',
            [$this, 'akipshoptetimport_show_content'],
            'dashicons-products',
            81
        );
    }

    function akipshoptetimport_show_content()
    {
        include(dirname(__FILE__) . "/Model/Category.php");
        include(dirname(__FILE__) . "/Repository/CategoryRepository.php");
        include(dirname(__FILE__) . "/admin/view/" . 'list.php');
    }

    public function custom_post_type()
    {
        register_post_type('category', [
            'public' => true,
            'label' => esc_html__('Kategorie', 'akipshoptetimport'),
            'supports' => ['title', 'thumbnail'],
            'has_archive' => true,
            'menu_icon' => 'dashicons-list-view',
            'menu_position' => 80
        ]);
        add_action('add_meta_boxes', [$this, 'akipshoptetimport_metabox']);
    }

    public function akipshoptetimport_metabox()
    {
        add_meta_box(
            'akipshoptetimport_metabox',
            'Doplňující info',
            [$this, 'akipshoptetimport_render_metabox'],
            'category',
            'normal'
        );
    }

    public function akipshoptetimport_render_metabox($post)
    {
        $value = get_post_meta($post->ID, 'shoptet_url', true);
        echo '<label for="akipshoptetimport_category_shoptet_url">Adresa XML feedu</label>';
        echo "<input type='text' id='akipshoptetimport_category_shoptet_url' name='shoptet_url' placeholder='vložte url adresu xml feedu' value='{$value}'>";
    }

    public function akipshoptetimport_save($post_id)
    {
        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
            return;
        }
        $shotet_url = $_POST['shoptet_url'];
        update_post_meta($post_id, 'shoptet_url', $shotet_url);
        if (file_get_contents($shotet_url)) {
            $this->readXml($shotet_url, $post_id);
        }
    }

    private function readXml($url, $post_id)
    {
        global $wpdb;
        $woocommerce = new Client(
            self::_URL,
            self::CONSUMER_KEY,
            self::CONSUMER_SECRET,
            [
                'wp_api' => true,
                'version' => 'wc/v3',
                'timeout' => 0
            ]
        );
        $post = get_post($post_id);
        $category = get_term_by( 'name', $post->post_title, 'product_cat' );
        if ($category->term_id) {
            $products = wc_get_products(array(
                'category' => array($category->slug),
                'posts_per_page' => -1 //select all products
            ));
            $productsId = array_map(function ($item) {
                return $item->id;
            }, $products);
            $woocommerce->post('products/batch', [
                'delete' => $productsId
            ]);
        } else {
            wp_insert_term( $post->post_title, 'product_cat', []);
            $category = get_term_by( 'name', $post->post_title, 'product_cat' );
        }
        $xml = simplexml_load_file($url);
        if ($xml) {
            foreach ($xml as $item) {
                $product = [
                    'name'          => (string)$item->NAME,
                    'type'          => 'external',
                    'description'   => (string)$item->DESCRIPTION,
                    'short_description'   => (string)$item->SHORT_DESCRIPTION,
                    'external_url'  => (string)$item->ORIG_URL,
                    'categories'    => [
                        [
                            'id' => $category->term_id,
                        ],
                    ],
                    'button_text'   => 'Otevřít E-shop'
                ];
                $images = $item->IMAGES->IMAGE;
                if ($images) {
                    if ($images->count() > 0) {
                        define('ALLOW_UNFILTERED_UPLOADS', true);
                        foreach ($images as $key => $image) {
                            $product['images'][] = ['src' => (string)$image];
                        }
                    }
                }
                if (isset($item->VARIANTS) && $item->VARIANTS) {
                    $variants = $item->VARIANTS->VARIANT;
                    $priceArray = [];
                    $variantsHtml = "<br><table><thead><tr><th>Název</th><th>Cena</th></tr></thead><tbody>";
                    foreach ($variants as $variant) {
                        $parameters = $variant->PARAMETERS->PARAMETER;
                        if ($parameters->count() > 1) {
                            $parameters = $parameters->children()[0];
                        }
                        $priceArray[] = (float)$variant->PRICE_VAT;
                        $variantsHtml .= "<tr><td>{$parameters->NAME} {$parameters->VALUE}</td><td>{$variant->PRICE_VAT} Kč</td></tr>";
                    }
                    $variantsHtml .= "</tbody></table>";
                    $minPrice = min($priceArray);
                    $product['regular_price'] = (string)$minPrice;
                    $product['description'] =(string)$item->DESCRIPTION . $variantsHtml;
                } else {
                    $product['regular_price'] = (string)$item->PRICE_VAT;
                }
                $woocommerce->post('products', $product);
            }
        }
    }

    // connect CSS & JS
    public function akipshoptetimport_load_assets_admin()
    {
        wp_enqueue_style('akipshoptetimportStyle', plugins_url('/assets/admin/style.css', __FILE__));
        wp_enqueue_script('akipshoptetimportScript', plugins_url('/assets/admin/main.js', __FILE__));
        wp_enqueue_script('jquery');
    }

    public function akipshoptetimport_load_assets_front()
    {
        wp_enqueue_style('akipshoptetimportStyle', plugins_url('/assets/front/style.css', __FILE__));
        wp_enqueue_script('akipshoptetimportScript', plugins_url('/assets/front/main.js', __FILE__));
    }
}

if (class_exists('AkipShoptetImport')) {
    $app = new AkipShoptetImport();
    $app->register();
}
register_activation_hook(__FILE__, [$app, 'activation']);
//register_activation_hook(__FILE__, [$app, 'akipshoptetimport_create_plugin_database_table']);
register_deactivation_hook(__FILE__, [$app, 'deactivation']);


