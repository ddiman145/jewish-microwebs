<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'microsite' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'eJ5Htl7#G@(z!+;QbF]G$!$`>6C3@a1BYkZQZo(tnJxfkQp`[e/jY0y*yKv!Z3~/' );
define( 'SECURE_AUTH_KEY',  'r;o J.Hl.cPQ/-abfL)N;mw[E]DLVdD9-!gD(IR@UqM/][HS5`!0]}j(A(aXMnMz' );
define( 'LOGGED_IN_KEY',    'q%:W%93!Y8_[X/l:Z%f}s;!;SM[#/I][;N#> ?)gFTPhQc9pgIrwti/|B;AG,%:^' );
define( 'NONCE_KEY',        'S>pqqrHJ9,}TU8{3O0aTDDGbG`=!s;1Ew7$5dt)1G;6#`}R`)jCb5KgG.q%O~yn.' );
define( 'AUTH_SALT',        '}k.w?-2-%ovw]f3p%qLp{jf.c40k--JX@0Za&;yBJV]cVDfi1<8oE6b5&+*DN^Wx' );
define( 'SECURE_AUTH_SALT', '?gv(|I}.D^Ow_txxdj:uGeFG,FAA]OJrLu@[^!-_S#p7]KGAa3=mq<YhUGGVjaE9' );
define( 'LOGGED_IN_SALT',   'zLo~/CF-uz]V|Q qAd6c4KXq.y+2<nH?BO.x2tPE2^8.@6IEuHH`hrZ^osO$IQD`' );
define( 'NONCE_SALT',       '4ztRVJX[&];j(k),;{sG!0m}gD,.g#oUMwKPb@Bj?)NNgtUdo8 0|Y[IJ4l_Vw]5' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
